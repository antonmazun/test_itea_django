from modeltranslation.translator import translator, TranslationOptions


from client_app.models import Author


class AuthorTranslationOptions(TranslationOptions):
    """
    Класс настроек интернационализации полей модели Modelka.
    """

    fields = ('bio', 'type_view', 'pseudoname')


translator.register(Author, AuthorTranslationOptions)