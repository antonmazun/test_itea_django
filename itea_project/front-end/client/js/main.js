import postComponentJS from './post'


class Root {
    constructor() {
        this.post_component = new postComponentJS();
    }
}

var App = new Root();
window.App = App;

