from django.db import models


# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=255, verbose_name='Title')
    text = models.TextField(max_length=5000, verbose_name='Text')
    date = models.DateTimeField(auto_now_add=True)
    
    
    def __str__(self):
        return self.title  + ' ' + str(self.date)
