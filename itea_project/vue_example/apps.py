from django.apps import AppConfig


class VueExampleConfig(AppConfig):
    name = 'vue_example'
