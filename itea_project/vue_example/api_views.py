from django.core import serializers

from django.shortcuts import get_object_or_404
from rest_framework import viewsets, generics
from .serializers import PostSerializer
from rest_framework.response import Response
from .models import Post
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import status


class AllPost(APIView):
    def get(self, request, format=None):
        all_post = Post.objects.all()
        serializer = PostSerializer(all_post, many=True)
        return Response(serializer.data)


class PostDetailView(APIView):

    def get(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        serializer = PostSerializer(post)
        return Response(serializer.data)

    def delete(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        post.delete()
        content = {'status': True}
        return Response(content)
