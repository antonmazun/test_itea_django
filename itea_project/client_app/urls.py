from django.urls import path, include, re_path
from . import views , auth_views

app_name = 'client_app'
urlpatterns = [
    path('cabinet', views.cabinet, name='cabinet'),
    path('change_color', views.change_color, name='change_color'),
    path('set-to-wish-list', views.set_to_session, name='set-to-wish-list'),
    path('write_post', views.write_post, name='write_post'),
    path('my_articles', views.my_articles, name='my_articles'),
    path('wish-list/<int:pk>/', views.wish_list, name='wish-list'),
    path('article-item/<int:pk>',
         views.article_item,
         name='article-item'),
    path('add-commen/<int:article_id>' , views.add_comment , name='add_comment'),
    path('filter_by_status/<str:status>' ,
         views.filter_by_status ,
         name='filter_by_status'),
    path('delete_article/<int:pk>' , views.delete_article  , name='delete_article'),
    path('show_article/<int:pk>' , views.show_article  , name='show_article')

]

auth_urls = [
    path('', auth_views.login_, name='login'),
    path('register', auth_views.register_, name='register'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.activate, name='activate'),
    path('logout', auth_views.logout_, name='logout'),
]

urlpatterns += auth_urls
