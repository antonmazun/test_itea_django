from django import template

register = template.Library()


@register.filter
def custom_upper(value):
    return list(value)[2].upper()


@register.filter
def add_class(value):
    if value % 3 == 0:
        return 'three'
    elif value % 3 == 1:
        return 'two'
    elif value % 3 == 2:
        return 'one'

@register.filter
def check_sale(value , arg):
    if not arg:
        if value:
            return 'sale-product'
        else:
            return ''
    elif arg == 'price':
        return 'sale-price'


@register.filter
def new_price(old_price , percent):
    return old_price * ((100 - percent)/100)




from django.utils import translation
from django.urls import resolve, reverse

class TranslatedURL(template.Node):
    def __init__(self, language):
        self.language = language

    def render(self, context):
        view = resolve(context['request'].path)
        request_language = translation.get_language()
        translation.activate(self.language)
        print(view.args)
        url = reverse(view.url_name, args=view.args, kwargs=view.kwargs)
        translation.activate(request_language)
        context['request'].session[translation.LANGUAGE_SESSION_KEY] = request_language
        return url


@register.tag(name='translate_url')
def do_translate_url(parser, token):
    language = token.split_contents()[1]
    return TranslatedURL(language)
