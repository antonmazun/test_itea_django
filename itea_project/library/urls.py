from django.urls import path, include
from . import views
urlpatterns = [
    path('', views.index , name='home'),
    path('test/', views.test),
    path('about-us-and-my-project/', views.about , name='about'),
    path('best-book/', views.best_book),
    path('sales/', views.sales),
    path('article-by-user/', views.article_by_user , name='article-by-user'),
    path('to-do-list-project/' , views.to_do),
    path('info-book/<int:pk>' , views.info_book , name='info-book'),
    path('search' , views.search , name='search'),
]



calc_urls = [
    path('calc/' , views.calc)
]

urlpatterns += calc_urls



from django.conf import settings
from django.conf.urls.static import static

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


