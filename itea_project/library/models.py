from django.db import models
from django.utils.timezone import datetime
from PyPDF2 import PdfFileReader
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

import os


class Book(models.Model):
    GENRE = (
        ('programming', 'Программирование'),
        ('fantastic', 'Фантастика'),
        ('drama', 'Драма'),
        ('comedy', 'Комедия'),
    )

    title = models.CharField(max_length=255, verbose_name='Название книги')
    text = models.TextField(max_length=1000, verbose_name='Текст')
    price = models.FloatField(verbose_name='Цена')
    image = models.ImageField(upload_to='image_book', default='', blank=True, null=True)
    create_at = models.DateTimeField(default=datetime.now)
    document = models.FileField(upload_to='documents/', default='', blank=True, null=True)
    genre = models.CharField(max_length=50, choices=GENRE, default='')
    is_sale = models.BooleanField(default=False)
    percent_sale = models.FloatField(validators=[MinValueValidator(0.0),
                                                 MaxValueValidator(100.0)],
                                     default=0.0)

    def __str__(self):
        return self.title
