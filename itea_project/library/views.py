from django.shortcuts import render , redirect
from django.http import HttpResponse
from .models import Book

from client_app.models import Article

# Create your views here.


def index(request):
    ctx = {}
    ctx['all_books'] = Book.objects.all().order_by('-create_at')
    ctx.update({'x': 45})
    ctx['current_tab'] = 'main'
    ctx['select'] = Book.GENRE
    return render(request, 'library/main.html', ctx)


def test(request):
    return HttpResponse('test!!!!')


def about(request):
    ctx = {}
    ctx['current_tab'] = 'about'
    return render(request, 'library/about.html', ctx)


def calc(request):
    ctx = {}
    calc_obj = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '/': lambda a, b: a / b,
        '*': lambda a, b: a * b,
    }
    ctx['current_tab'] = 'calc'
    ctx['operations'] = list(calc_obj.keys())

    if request.method == 'POST':
        try:
            a = int(request.POST.get('first'))
            oper = request.POST.get('oper')
            b = int(request.POST.get('second'))
            res = calc_obj[oper](a, b)
            ctx.update({
                'res': res
            })
        except (ValueError, KeyError) as e:
            ctx['error'] = e

    return render(request, 'calc.html', ctx)


def best_book(request):
    return render(request, 'library/best_book.html', {})


def sales(request):
    return render(request, 'library/sales.html', {})


def article_by_user(request):
    ctx  = {

    }
    ctx['all_articles'] = Article.objects.filter(status='publish')
    return render(request, 'library/article_by_user.html', ctx)


def to_do(request):
    return render(request, 'library/to_do_list.html', {})


from PyPDF2 import PdfFileReader

import sys, os
from itea_project import settings

sys.path.append('/media/')


def info_book(request, pk):
    book = Book.objects.get(pk=pk)
    ctx = {
        'book_instance': book,
        # 'count_page': PdfFileReader(open(os.path.join(book.document.path), "rb")).getNumPages()
    }
    try:
        ctx['count_page'] = PdfFileReader(open(os.path.join(book.document.path), "rb")).getNumPages()
    except Exception as e:
        ctx['error'] = 'Файла нет.'
    return render(request, 'library/book_info.html', ctx)


def search(request):
    ctx = {}
    if request.method == 'GET':
        try:
            q = request.GET.get('query')
            ctx['all_books'] = Book.objects.filter(genre=q)
            ctx['current_tab'] = 'main'
            ctx['select'] = Book.GENRE
            ctx['q'] = [elem[1]  for elem in Book.GENRE if elem[0] == q][0]
        except Exception as e:
            print(e)
            print(filter_arg)
            ctx['all_books'] = Book.objects.filter(**filter_arg)
        return render(request, 'library/main.html', ctx)
