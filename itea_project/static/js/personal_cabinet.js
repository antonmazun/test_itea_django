function delete_article() {
    $('.js-delete').click(function (e) {
        e.preventDefault();
        var ajax_url = $(this).attr('href');

        $.ajax({
            url: ajax_url,
            method: 'GET'
        }).done(function (response) {
            if (response.deleted) {
                alert('удаление прошло успешно!');
            }
            console.log(response);

            $('.js-article-reload').empty();
            $('.js-article-reload').append(response.response_html);
            $('.js-count').text(response.count_articles);
            delete_article();
        });
    })
}


function show_content_popup() {
    $('.js-show-content').click(function (e) {
        e.preventDefault();
        $('.popup-wrapper').show();

        $.ajax({
            url: $(this).attr('href'),
            method: 'GET'
        }).done(function(resp){

            $('.popup-wrapper').find('.content').html(resp.content);
            console.log(resp.content);
        }).fail(function(error){
            console.log('error');
            console.log(error);
        })
    })
}

function popup_close() {
    $('.js-close').click(function () {
        $('.popup-wrapper').hide();
    })
}


delete_article();

show_content_popup();

popup_close();